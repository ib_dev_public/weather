
export function gt(selector, scope) {
  return (scope || document).getElementsByTagName(selector);
}

export const escapeForHTML = s => s.replace(/[&<]/g, c => c === '&' ? '&amp;' : '&lt;');
