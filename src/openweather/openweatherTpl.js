
import {escapeForHTML} from '../helpers';


export default (items) => {
  return `<strong>${escapeForHTML(items.msg)}</strong>`
}


