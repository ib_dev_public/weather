
import {gt} from '../helpers';


function createList(data){

  let station = gt("station", data),
      name = gt("name", data),
      visibility = gt("visibility", data),
      airtemperature = gt("airtemperature", data),
      windspeed = gt("windspeed", data),
      windspeedmax = gt("windspeedmax", data),
      winddirection = gt("winddirection", data),
      list = [];

  for (let i = 0 ; i < station.length; i++) {
    list.push(
      {
        name: name[i].textContent,
        visibility: visibility[i].textContent,
        airtemperature: airtemperature[i].textContent,
        windspeed: windspeed[i].textContent,
        windspeedmax: windspeedmax[i].textContent,
        winddirection: winddirection[i].textContent
      }
    );
  }

  return list;
}


function sortList(list){

  let sortedList = [],
      stations = ['tallinn-harku', 'ravere', 'sauga', 'narva', 'hvi'],
      cities = ['Tallinn', 'Tartu', 'Pärnu', 'Narva', 'Jõhvi'],
      station = null;

  for (let i = 0 ; i < stations.length; i++) {
    station = list.find(
      (el) => {
        let str = el.name.trim().toLowerCase();
        return str.endsWith(stations[i]);
      }
    );
    station.city = cities[i];
    sortedList.push(station);
  }

  return sortedList;
}

function ilmateenistusExecutor(resolve, reject) {

  fetch('http://www.ilmateenistus.ee/ilma_andmed/xml/observations.php', {
    method: 'get'
  }).
  then(response => response.text()).
  then(str => (new window.DOMParser()).parseFromString(str, "text/xml")).
  then(createList).
  then(sortList).
  then(sorted => {
    resolve(sorted);
  }).
  catch(err => {
    console.log('error: ', err);
  });

}

export default ilmateenistusExecutor;
