
import {escapeForHTML} from '../helpers';

function repeater(items) {
  return items.list.reduce((a, item) => a + `
        <ul class="city">
          <li class="city__item"><strong>${escapeForHTML(item.city)}</strong></li>
          <li class="city__item">Pilvisus: ${escapeForHTML(item.visibility)}</li>
          <li class="city__item">Temperatuur: ${escapeForHTML(item.airtemperature)} °C</li>
          <li class="city__item">Tuuletugevus: ${escapeForHTML(item.windspeed)} m/s</li>
          <li class="city__item">Maksimaalne tuuletugevus: ${escapeForHTML(item.windspeedmax)} m/s</li>
          <li class="city__item">Tuulesuund: <img src="arrow-up.jpg" height="13" width="13"  style="transform: rotate( ${escapeForHTML(item.winddirection)}deg)"></li>
        </ul>
  `, '');
}


export default (items) => {
  return `<div class="cities-list">${repeater(items)}</div>`
}


