
export class Router {
  constructor(id) {
    this.routes = {};
    this.el = document.getElementById(id) || document.getElementById('view');

    window.addEventListener('hashchange', this.loadView.bind(this));
    window.addEventListener('load', this.loadView.bind(this));
  }

  route(path, template, controller, promise){
    this.routes[path] = {template: template, controller: controller, promise: promise};
  }

  _tmpl(template, data) {
    return template(data);
  }

  _showView(el, route, data){
    if (el && route.controller) {
      el.innerHTML = this._tmpl(route.template, new route.controller(data));
    }
  }

  loadView() {
    let el = this.el;
    let url = location.hash.slice(1) || '/';
    let route = this.routes[url];

    if(route.promise){
      let promise = new Promise(route.promise);
      promise.then(
        (data) => {
          this._showView(el, route, data);
        })
        .catch(
        (reason) => {
          console.log('Rejected promise: ', reason);
          this._showView(el, route, reason);
        }
      );
    } else {
      this._showView(el, route);
    }

  }

}




