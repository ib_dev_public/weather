
import ilmateenistusTpl from './ilmateenistus/ilmateenistusTpl';
import ilmateenistusService from './ilmateenistus/ilmateenistusService';
import {IlmateenistusCtrl} from './ilmateenistus/ilmateenistusCtrl';

import openweatherTpl from './openweather/openweatherTpl';
import openweatherService from './openweather/openweatherService';
import {OpenweatherCtrl} from './openweather/openweatherCtrl';


import {Router} from './router';
let router = new Router('main-view');



document.addEventListener("DOMContentLoaded", (event) => {

  router.route('/', ilmateenistusTpl, IlmateenistusCtrl, ilmateenistusService);
  router.route('/openweather', openweatherTpl, OpenweatherCtrl, openweatherService);

  window.setInterval(
    () => {
      router.loadView(); // make new http request and refresh currently loaded view
    },
  1800000);

});











